// 1. In một bảng số từ 1-100 
        function handleBang(){
            var numbers = [];
            let a=1
            const mang=document.querySelector("#mang")
            // Lặp theo hàng
            for (var i = 0; i < 10; i++){
                numbers[i] = [];
                // Lặp theo cột, số cộ từ 0 -> số lượng phần tử của hàng i
                for (var j = 0; j < 10; j++){
                    numbers[i][j] = a++
                }
            }
            let str1=""
            for(var i=0;i<10;i++){
                str1+=`<br/>`
                for(let j=0;j<10;j++){
                    str1+=`00${numbers[i][j]}  `.slice(-5)
                }
            }
            mang.innerHTML=str1
        }
        
// 2. Viết function nhận vào tham số là một mảng số nguyên,tìm và in ra các số nguyên tố trong mảng
    let arrA=[]
        const soNguyenToArr=[]
        function createArr(){
            const arr=document.querySelector("#arr")
            const number=document.querySelector(".number").value*1
            if(document.querySelector(".number").value.trim()==""){
                return
            }else{
                arrA.push(number)
            }
            document.querySelector(".number").value=""
            arr.innerHTML=arrA
        }
        
        function soNguyenTo(number){
            if(number<2){return false}
            if(number==2){return true}
            if(number%2==0){return false}
            else if(number>2){
                for(let i=3;i<number-1;i+=2){
                    if(number%i==0){
                        return false
                    }else{
                        return true
                    }
                }
            }else{
                return false
            }
        }
        function soNguyenToTrongMang(){
            const result=document.querySelector("#result")
            
            for(let i=0;i<arrA.length;i++){
                if(soNguyenTo(arrA[i])){
                    soNguyenToArr.push(arrA[i])
                }
            }
            result.innerHTML=soNguyenToArr
        }
        
        // 3.Viết function nhận vào tham số n, tính S=(2+3+4...+n)+2n
        function handleClick(){
            const number3=document.querySelector(".number3").value*1
            const result3=document.querySelector("#result3")
            let sum =0
            if(number3>=2){
                for(let i=2;i<=number3;i++){
                    sum+=i
                }
                console.log(sum)
                result3.innerHTML=sum+2*number3
            }else return
        }

        //4.Viết function nhận vào tham số n, tính số lượng ước số của n 
        function tinhUocSo(){
            const result4=document.getElementById("result4")
            const number4=document.querySelector(".number4").value*1
            let arr=[]
            for(let i=0;i<=number4;i++){
                if(number4%i==0){
                    arr.push(i)
                }
            }
            result4.innerHTML=arr
        }

        //5.Viết chương trình tìm số đảo ngược của 1 số nguyên dương n nhập từ bàn phím
        function xuLyDaoNguoc(){
            const result5=document.getElementById("result5")
            const number5=document.querySelector(".number5").value
            const a=number5.split("").reverse().join("")
            result5.innerHTML=a
        } 
        //6.Tìm x nguyên dương lớn nhất, biết 1+2+3+...+x ≤100
        function timNguyenDuong(){
            const result10=document.querySelector("#result10")
            let sum=0
                for(let i=0; sum<=100;i++){
                    sum+=i
                    result10.innerHTML=i-1
                } 
       } 

       //7.Viết function nhận vào số n, in ra bảng cửu chương tương ứng với số đó.
       function bangCuuChuong(){
        const number7=document.querySelector(".number7").value*1
        const result7=document.getElementById("result7")
        let str=""
        for(let i=0;i<11;i++){
            str+=`${number7} x ${i} = ${number7*i}<br/>`
        }
        result7.innerHTML=str
    }

    //8.Viết hàm chia bài cho 4 người chơi.
    function chiaBai(){
        const cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S","AS", "7H", "9K", "10D"]
        const player1=document.getElementById("player1")
        const player2=document.getElementById("player2")
        const player3=document.getElementById("player3")
        const player4=document.getElementById("player4")

        let player1Arr=[]
        let player2Arr=[]
        let player3Arr=[]
        let player4Arr=[]

        for(let i=0;i<cards.length;i+=4){
            player1Arr.push(cards[i])
        }
        player1.innerHTML=player1Arr

        for(let i=1;i<cards.length;i+=4){
            player2Arr.push(cards[i])               
        }
        player2.innerHTML=player2Arr

        for(let i=2;i<cards.length;i+=4){
            player3Arr.push(cards[i])               
        }
        player3.innerHTML=player3Arr

        for(let i=3;i<cards.length;i+=4){
            player4Arr.push(cards[i])               
        }
        player4.innerHTML=player4Arr           
    }

    //9.Tìm số gà, số chó khi số tổng số con là m và tổng số chân là n

    function tinhChoGa(){
        const totalAnimal=document.querySelector(".totalAnimal").value*1
        const totalLeg=document.querySelector(".totalLeg").value*1
        const result9=document.getElementById("result9")
        result9.innerHTML=`Số chó là:${(totalLeg-totalAnimal*2)/2} ;Số gà là: ${totalAnimal-(totalLeg-totalAnimal*2)/2}`
    }

    //10. Nhập vào số giờ và số phút => góc lệch giữa kim giờ và kim phút.
    function tinhGoc(){
        const hours=document.querySelector(".hours").value*1
        const minutes=document.querySelector(".minutes").value*1
        const result11=document.getElementById("result11")
        const deg=Math.sqrt((minutes*6-0.5*(hours*60+minutes))*(minutes*6-0.5*(hours*60+minutes)))
        result11.innerHTML=deg + "deg"
    }   



